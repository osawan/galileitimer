let btnStart = document.getElementById('btnStart');
let btnCancel = document.getElementById('btnCancel');
let timer = document.getElementById('timer');
let currentBar = document.getElementById('currentBar');
var countdownId;
let progressBar = document.getElementById('progressBar');

//スタートボタン押下時処理
btnStart.addEventListener('click', function(){
  let time = document.getElementsByName('time')[0].value;
  if(!isNaN(time) && time > 0){ //0より大きい数値の場合のみ許可
    changeDisplayStyle("settingUI", "none"); //settingUIを非表示
    changeDisplayStyle("displayUI", "flex"); //displayUIを表示
    startCountdown(time); //カウントダウン処理
  } else {
    alert("有効な値を入力してください");
  }
});

//キャンセルボタン押下
btnCancel.addEventListener('click', function(){
  clearTimeout(countdownId); //countdownを停止
  changeDisplayStyle("displayUI", "none");
  changeDisplayStyle("settingUI", "flex");
  progressBar.style.backgroundImage = "url('img/bg1.png')";
  currentBar.style.backgroundColor = "#2EB872";
});

function startCountdown(time){
  let curSec = 60*time;
  let totalSec = 60*time;
  let currentBarWidth = 100;
  var countdown = function(){
    curSec --;
    let sec = curSec % 60;
    let min = Math.floor(curSec / 60);

    let currentTime = min + ":" + sec;
    if (sec < 10){ //秒が1桁の場合は10の位に0を追加
      currentTime = min + ":0" + sec;
    }
    // console.log(currentTime);
    timer.innerHTML = currentTime; //UIに時間を反映

    //バーの長さ
    currentBarWidth -= 100 / totalSec;
    currentBar.style.width = currentBarWidth+"%";

    //背景画像変更
    if(currentBarWidth < 50 && currentBarWidth > 31){
      progressBar.style.backgroundImage = "url('img/bg2.png')";
      currentBar.style.backgroundColor = "#E8B844";
    } else if (currentBarWidth < 30 && currentBarWidth > 11) {
      progressBar.style.backgroundImage = "url('img/bg3.png')";
      currentBar.style.backgroundColor = "#FF8F56";
    } else if (currentBarWidth < 10) {
      progressBar.style.backgroundImage = "url('img/bg4.png')";
      currentBar.style.backgroundColor = "#DF6A6A";
    }

    console.log(currentBarWidth);

    countdownId = setTimeout(countdown, 1000); //0になったらタイマーを削除
    if (curSec == 0){
      currentBar.style.width = 0;
      clearTimeout(countdownId);
    }
  }
  countdown();
}

function changeDisplayStyle(target_ele, value){
  let target = document.getElementsByClassName(target_ele);
  for(let i = 0; i < target.length; i++){
    target[i].style.display = value;
  }
}
