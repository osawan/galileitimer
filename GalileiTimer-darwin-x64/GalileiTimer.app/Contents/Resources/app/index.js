const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const Menu = electron.Menu;

let mainWindow;

app.on('ready', () => {
  createWindow();
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow();
  } else {
    mainWindow.setIgnoreMouseEvents(false);
  }
});

function createWindow() {
  let settings = {
    width: 400,//+0
    height: 82,//+2
    // transparent: true,
    // frame: false,
    resizable: false,
    alwaysOnTop: true,
    hasShadow: false
  };

  mainWindow = new BrowserWindow(Object.assign(settings));
  mainWindow.loadURL(`file://${__dirname}/index.html`);

  //chromeの開発ツールを起動
  // mainWindow.webContents.openDevTools();

  mainWindow.on('closed', function () {
    mainWindow = null;
  });
}
